﻿using UnityEngine;

public class StaticObjects : MonoBehaviour
{
    public static GameObject TargetEnemy { get; set; }
    public static GameObject Player { get; set; }
    public static Vector3 PlayerLastPosition { get; set; }
    private void Awake()
    {
        Player = GameObject.FindGameObjectWithTag(Tags.Player);
    }
}
