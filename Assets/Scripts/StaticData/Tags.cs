﻿public class Tags
{
    public static string Player { get; } = "Player";
    public static string Ground { get; } = "Ground";
    public static string Bullet { get; } = "Bullet";
    public static string Enemy { get; } = "Enemy";
    public static string Border { get; } = "Border";
    public static string StoneWall { get; } = "StoneWall";
    public static string Finish { get; } = "Finish";
    public static string Platform { get; } = "Platform";
    
}
