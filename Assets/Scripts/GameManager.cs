﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    GameObject gameOverScreen;

    [SerializeField]
    GameObject endGameScreen;

    private Vector3 gameOverScreenShowingPosition, endGameScreenShowingPosition;
    public static GameManager INSTANCE;
    private void Start()
    {
        gameOverScreenShowingPosition = new Vector2(15, 100);
        endGameScreenShowingPosition = new Vector2(15, 100);
        INSTANCE = GetComponent<GameManager>();
    }
    public void GameOver()
    {
        gameOverScreen.SetActive(true);
        StartCoroutine(ShowScreen(gameOverScreen, gameOverScreenShowingPosition));
    }
    private IEnumerator ShowScreen(GameObject screen, Vector3 showingPosition)
    {
        while (screen.transform.position != gameOverScreenShowingPosition)
        {
            screen.transform.localPosition = Vector3.MoveTowards(screen.transform.localPosition, showingPosition, 20f);
            yield return new WaitForEndOfFrame();
        }
        yield break;
    }
    public static IEnumerator HideWall()
    {
        GameObject wall = GameObject.FindGameObjectWithTag(Tags.StoneWall);
        while (true)
        {
            wall.transform.localScale -= 0.05f * Vector3.up;
            if (wall.transform.localScale.y < 0.3f)
            {
                wall.transform.localScale = Vector2.zero;
                yield break;
            }
            yield return new WaitForEndOfFrame();
        }
    }
    public void Play()
    {
        SceneManager.LoadScene(1);
    }
    public void Quit()
    {
        Application.Quit();
    }
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void Menu()
    {
        SceneManager.LoadScene(0);
    }
    public void NextLevel()
    {
        if (SceneManager.GetActiveScene().buildIndex < SceneManager.sceneCountInBuildSettings - 1)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        else
        {
            End();
        }
    }
    private void End()
    {
        endGameScreen.SetActive(true);
        StartCoroutine(ShowScreen(endGameScreen, endGameScreenShowingPosition));
        Debug.Log("END");
    }
}
