﻿using System.Collections;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    [HideInInspector]
    public float minDamage, maxDamage;

    private bool flag = false;
    private void Start()
    {
        StartCoroutine(BulletToTarget(StaticObjects.Player.transform.position));
        flag = true;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(Tags.Player))
        {
            Player.INSTANCE.TakeDamage(Random.Range(minDamage, maxDamage));
            Enemy.INSTANCE.enemyBulletHit.transform.position = transform.position;
            Enemy.INSTANCE.enemyBulletHit.Emit(15);
            gameObject.SetActive(false);
        }
        else if (!collision.CompareTag(Tags.Enemy) && !collision.CompareTag(Tags.Bullet) && !collision.CompareTag(Tags.Platform))
        {
            gameObject.SetActive(false);
        }
    }
    private IEnumerator BulletToTarget(Vector3 target)
    {
        var direction = target - transform.position; //Actually enemy position 
        transform.right = direction;
        while (true)
        {
            transform.Translate(Vector2.right * Time.deltaTime * 3f);
            yield return new WaitForEndOfFrame();
        }
    }
    private void OnEnable()
    {
        if (flag)
            StartCoroutine(BulletToTarget(StaticObjects.Player.transform.position));
    }
}
