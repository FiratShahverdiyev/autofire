﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private float healthScaleFactor;
    private Rigidbody2D rb;
    private int index;
    private bool isWaiting;
    private Transform destination;
    private Animator animator;

    [SerializeField]
    EnemyType enemyType;

    [Header("Particle Effects")]
    public ParticleSystem enemyBulletHit;
    [SerializeField]
    ParticleSystem enemySuicide;
    [SerializeField]
    ParticleSystem enemyDead;


    [SerializeField]
    float health;

    [SerializeField]
    List<Transform> patrolPoints;

    [SerializeField]
    GameObject bulletPrefab;

    [SerializeField]
    int initializeMagazineSize;

    [SerializeField]
    float minDamage, maxDamage;



    private GameObject healthBar;
    private float healthBarScale;
    private SpriteRenderer spriteRenderer;
    private List<GameObject> bullets;
    private bool isFire;
    public static Enemy INSTANCE { get; set; }
    private void Start()
    {
        if (enemyType.Equals(EnemyType.FROG))
            animator = GetComponent<Animator>();
        healthBar = transform.GetChild(0).gameObject;
        healthBarScale = healthBar.transform.localScale.x;
        healthScaleFactor = health;
        spriteRenderer = GetComponent<SpriteRenderer>();
        isWaiting = false;
        index = 0;
        INSTANCE = gameObject.GetComponent<Enemy>();
        bullets = new List<GameObject>();
        isFire = false;
        InitializeMagazinePool();
        rb = GetComponent<Rigidbody2D>();
        if (patrolPoints.Count > 0)
        {
            destination = patrolPoints[index];
        }
    }
    private void Update()
    {
        CheckEnemyType();
        if (destination != null && !isWaiting)
        {
            Patrol();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (enemyType.Equals(EnemyType.TRIANGLE))
        {
            if (!collision.collider.CompareTag(Tags.Bullet) && health <= 0)
            {
                enemyDead.transform.position = transform.position;
                enemyDead.Emit(50);
                Destroy(gameObject);
            }
        }
    }
    public void TakeDamage(float damage)
    {
        health -= damage;
        healthBar.transform.localScale -= new Vector3(damage * healthBarScale / healthScaleFactor, 0f);
        if (health <= 0)
        {
            healthBar.transform.localScale = Vector3.zero;
            Dead();
        }
    }
    private void Dead()
    {
        rb.gravityScale = 1;
        if (enemyType.Equals(EnemyType.FROG))
        {
            enemyDead.transform.position = transform.position;
            enemyDead.Emit(50);
            Destroy(gameObject);
        }
    }
    private void Fire()
    {
        StaticObjects.PlayerLastPosition = StaticObjects.Player.transform.position;
        bool haveBullet = false;
        GameObject shootingBullet = null;
        foreach (GameObject bullet in bullets)
        {
            if (bullet.activeSelf == false)
            {
                shootingBullet = bullet;
                haveBullet = true;
                break;
            }
        }
        if (haveBullet)
        {
            shootingBullet.transform.position = transform.position;
            EnemyBullet enemyBullet = shootingBullet.GetComponent<EnemyBullet>();
            enemyBullet.maxDamage = maxDamage;
            enemyBullet.minDamage = minDamage;
            shootingBullet.SetActive(true);
        }
        else
        {
        }
        isFire = false;
    }
    private void InitializeMagazinePool()
    {
        for (int i = 0; i < initializeMagazineSize; i++)
        {
            GameObject bullet = Instantiate(bulletPrefab);
            bullet.transform.position = transform.position;
            bullets.Add(bullet);
        }
    }
    private void Patrol()
    {
        if (enemyType.Equals(EnemyType.FROG))
        {
            StartCoroutine(CompareXAxis());
        }
        if (Vector2.Distance(transform.position, destination.position) > 1.5f)
        {
            if (enemyType.Equals(EnemyType.FROG))
                animator.SetFloat(StaticFIelds.VelocityX, 1f);
            transform.position = Vector2.MoveTowards(transform.position, destination.position, 2f * Time.deltaTime);
        }
        else
        {
            if (index < patrolPoints.Count - 1)
                destination = patrolPoints[++index];
            else
            {
                index = 0;
                destination = patrolPoints[index];
            }
            if (!isWaiting)
            {
                if (enemyType.Equals(EnemyType.FROG))
                    animator.SetFloat(StaticFIelds.VelocityX, 0f);
                isWaiting = true;
                Invoke(nameof(WaitOnDestination), 4f);
            }
        }
    }
    private IEnumerator CompareXAxis()
    {
        float currentX = transform.position.x;
        yield return new WaitForSeconds(0.1f);
        if (currentX < transform.position.x)
        {
            spriteRenderer.flipX = false;
        }
        else if (currentX > transform.position.x)
        {
            spriteRenderer.flipX = true;
        }
    }
    private void WaitOnDestination()
    {
        isWaiting = false;
    }
    private void CheckEnemyType()
    {
        switch (enemyType)
        {
            case EnemyType.TRIANGLE:
                {
                    if (!isFire)
                    {
                        isFire = true;
                        Invoke(nameof(Fire), 3f);
                    }
                    break;
                }
            case EnemyType.FROG:
                {
                    if (Vector2.Distance(transform.position, StaticObjects.Player.transform.position) < 2f)
                    {
                        enemySuicide.transform.position = transform.position;
                        Destroy(gameObject);
                        enemySuicide.Emit(15);
                    }
                    break;
                }
        }
    }
}
