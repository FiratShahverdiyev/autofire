﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Player : MonoBehaviour
{
    private Rigidbody2D rb;
    private Animator animator;
    public SpriteRenderer SpriteRenderer { get; set; }
    private float directionX;
    private bool isGround;
    private GameObject healthBar;
    private float healthBarScale;
    private float health;
    private float healthScaleFactor;

    [SerializeField]
    ParticleSystem playerDead;
    public static Player INSTANCE { get; set; }

    private void Start()
    {
        healthBar = transform.GetChild(0).gameObject; //First child of player must be healtBar
        healthBarScale = healthBar.transform.localScale.x;
        health = 120f;
        healthScaleFactor = health;
        SpriteRenderer = GetComponent<SpriteRenderer>();
        isGround = true;
        INSTANCE = GetComponent<Player>();
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        directionX = CrossPlatformInputManager.GetAxis("Horizontal");
        Move();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag(Tags.Ground) || collision.collider.CompareTag(Tags.Platform))
        {
            isGround = true;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.CompareTag(Tags.Ground) || collision.collider.CompareTag(Tags.Ground))
        {
            isGround = false;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(Tags.Finish))
        {
            GameManager.INSTANCE.NextLevel();
        }
    }
    private void OnParticleCollision(GameObject other)
    {
        animator.SetTrigger(StaticFIelds.Hit);
        TakeDamage(Random.Range(5, 15));
    }
    private void Move()
    {
        if (directionX > 0.5f)
        {
            if (Weapon.INSTANCE.IsEnd)
            {
                Weapon.INSTANCE.transform.position = Weapon.INSTANCE.rightHand.position;
                SpriteRenderer.flipX = false;
            }
            rb.velocity = new Vector2(3, rb.velocity.y);
            animator.SetFloat(StaticFIelds.VelocityX, 1f);
        }
        else if (directionX < -0.5f)
        {
            if (Weapon.INSTANCE.IsEnd)
            {
                Weapon.INSTANCE.transform.position = Weapon.INSTANCE.leftHand.position;
                SpriteRenderer.flipX = true;
            }
            rb.velocity = new Vector2(-3, rb.velocity.y);
            animator.SetFloat(StaticFIelds.VelocityX, 1f);
        }
        else
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
            animator.SetFloat(StaticFIelds.VelocityX, 0f);
        }
        
    }
    public void Jump()
    {
        if (isGround)
        {
            rb.velocity = new Vector2(rb.velocity.x, 8);
            animator.SetTrigger(StaticFIelds.Jump);
            isGround = false;
        }
    }
    public void TakeDamage(float damage)
    {
        animator.SetTrigger(StaticFIelds.Hit);
        health -= damage;
        healthBar.transform.localScale -= new Vector3(damage * healthBarScale / healthScaleFactor, 0f);
        if (health <= 0)
        {
            healthBar.transform.localScale = Vector3.zero;
            Dead();
        }
    }
    private void Dead()
    {
        playerDead.transform.position = transform.position;
        playerDead.Emit(50);
        gameObject.SetActive(false);
        GameManager.INSTANCE.GameOver();
    }
}
