﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    private Player player;
    private bool isFire;
    private bool haveEnemy;
    public bool IsEnd { get; set; }
    public Transform leftHand, rightHand;
    private int magazinePoolInitializeSize;

    [SerializeField]
    float maxDamage, minDamage;

    [SerializeField]
    Transform bulletSpawnPosition;

    [SerializeField]
    GameObject enemies;

    [SerializeField]
    GameObject bulletPrefab;

    public ParticleSystem bulletHit;

    List<GameObject> bullets;
    public static Weapon INSTANCE { get; set; }
    private void Start()
    {
        if (enemies != null)
            haveEnemy = true;
        else
            haveEnemy = false;
        isFire = false;
        IsEnd = false;
        INSTANCE = GetComponent<Weapon>();
        bullets = new List<GameObject>();
        magazinePoolInitializeSize = 5;
        player = GameObject.FindWithTag(Tags.Player).GetComponent<Player>();
        InitializeMagazinePool();
    }

    private void Update()
    {
        if (IsEnd)
        {
            return;
        }
        if (!isFire && haveEnemy)
        {
            isFire = true;
            Invoke(nameof(Fire), 1f);
        }
        if (StaticObjects.TargetEnemy != null)
            transform.right = StaticObjects.TargetEnemy.transform.position - transform.position;
        if (transform.rotation.eulerAngles.z > 95f && transform.rotation.eulerAngles.z < 175f)
        {
            transform.position = leftHand.position;
            player.SpriteRenderer.flipX = true;
        }
        else if (transform.rotation.eulerAngles.z < 80f && transform.rotation.eulerAngles.z > 0f)
        {
            transform.position = rightHand.position;
            player.SpriteRenderer.flipX = false;
        }
    }
    private void Fire()
    {
        GameObject enemy = GetClosestEnemy();
        if (enemy == null)
        {
            StartCoroutine(GameManager.HideWall());
            IsEnd = true;
        }
        else
        {
            StaticObjects.TargetEnemy = enemy;
            GetBullet();
        }
        isFire = false;
    }
    public void Damage()
    {
        float damage = Random.Range(minDamage, maxDamage);
        if (StaticObjects.TargetEnemy != null)
            StaticObjects.TargetEnemy.GetComponent<Enemy>().TakeDamage(damage);
    }
    private GameObject GetClosestEnemy()
    {
        int enemyCount = enemies.transform.childCount;
        if (enemyCount == 0)
        {
            return null;
        }
        float closestDistance = float.MaxValue;
        GameObject closestEnemy = null;
        for (int i = 0; i < enemyCount; i++)
        {
            Transform enemy = enemies.transform.GetChild(i);
            float distanceToEnemy = Vector2.Distance(transform.position, enemy.position);
            if (distanceToEnemy < closestDistance)
            {
                closestEnemy = enemy.gameObject;
                closestDistance = distanceToEnemy;
            }
        }
        if (closestEnemy == null)
        {
            return null;
        }
        return closestEnemy;
    }
    private GameObject GetBullet()
    {
        bool haveBullet = false;
        GameObject shootingBullet = null;
        foreach (GameObject bullet in bullets)
        {
            if (bullet.activeSelf == false)
            {
                shootingBullet = bullet;
                haveBullet = true;
                break;
            }
        }
        if (haveBullet)
        {
            shootingBullet.SetActive(true);
            shootingBullet.transform.position = bulletSpawnPosition.position;
        }
        else
        {
            foreach (GameObject bullet in bullets)
            {
                bullet.SetActive(false);
            }
        }
        return shootingBullet;
    }
    private void InitializeMagazinePool()
    {
        for (int i = 0; i < magazinePoolInitializeSize; i++)
        {
            GameObject bullet = Instantiate(bulletPrefab);
            bullets.Add(bullet);
        }
    }

}
