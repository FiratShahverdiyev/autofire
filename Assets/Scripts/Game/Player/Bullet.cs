﻿using System.Collections;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private bool flag = false;
    private void Start()
    {
        StartCoroutine(BulletToTarget(StaticObjects.TargetEnemy.transform.position));
        flag = true;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(Tags.Enemy))
        {
            Weapon.INSTANCE.bulletHit.transform.position = transform.position;
            Weapon.INSTANCE.bulletHit.Emit(10);
            Weapon.INSTANCE.Damage();
            gameObject.SetActive(false);
        }
        else if (!collision.CompareTag(Tags.Player) && !collision.CompareTag(Tags.Bullet) && !collision.CompareTag(Tags.Platform))
        {
            gameObject.SetActive(false);
        }
    }
    private IEnumerator BulletToTarget(Vector3 target)
    {
        var direction = target - StaticObjects.Player.transform.position;
        transform.right = direction;
        while (true)
        {
            transform.Translate(Vector2.right * Time.deltaTime * 10f);
            yield return new WaitForEndOfFrame();
        }
    }
    private void OnEnable()
    {
        if (flag)
            StartCoroutine(BulletToTarget(StaticObjects.TargetEnemy.transform.position));
    }
}
